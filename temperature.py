import requests
import pandas as pd
import io
import  matplotlib.pyplot as plt
csv_data = requests.get("https://data.weather.gov.hk/weatherAPI/opendata/opendata.php?dataType=CLMMAXT&year=2019&rformat=csv&station=HKO").text
csv_data=io.StringIO(csv_data)
data = pd.read_csv(csv_data,skiprows=[0,1])
data = data.drop(list(range(0, data["年/Year"].count())[-5:]))
data.columns=["Year","Month","Day","Value","data Completeness"]
data["Month"] = data["Month"].astype("int")
data["Day"] = data["Day"].astype("int")
data = data[data['data Completeness']=='C']
data.head()
data.drop("data Completeness",axis=1,inplace=True)
data.head()
data['Date'] = data['Year'].astype(str) + '-' + data['Month'].astype(str) + '-' + data['Day'].astype(str)
data['Date'] = pd.to_datetime(data['Date'])
data.index = data['Date']
data.drop("Date",axis=1,inplace=True)
data.head()
plot = data["Value"].plot()
plot.set_title("Daily Maximum Temperature (°C) at the HKO")
plt.ylabel("Temperature (°C)", fontsize = 18)
plt.xlabel("Date", fontsize = 18)
plt.show()
plot = data["Value"].plot(kind="hist",bins=40)
plot.set_title("Historgram of Daily Maximum Temperature (°C) at the HKO")
plt.ylabel("Day", fontsize = 18)
plt.xlabel("Temperature (°C)", fontsize = 18)
plt.show()